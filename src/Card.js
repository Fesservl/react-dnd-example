import React from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import {getEmptyImage} from 'react-dnd-html5-backend';
import {DragSource, DropTarget} from 'react-dnd';
import flow from 'lodash/flow';
import SubCard from './SubCard';

const style = {
    border: '1px dashed gray',
    padding: '0.5rem 1rem',
    marginBottom: '.5rem',
    backgroundColor: 'white'
};

const cardSource = {
    beginDrag(props, monitor, component) {
        const hoverBoundingRect = (findDOMNode(
            component,
        )).getBoundingClientRect();
        return {
            id: props.id,
            index: props.index,
            text: props.text,
            width: hoverBoundingRect.width,
            left: hoverBoundingRect.left
        };
    }
};

const cardTarget = {
    hover(props, monitor, component) {
        const dragIndex = monitor.getItem().index;
        const hoverIndex = props.index;


        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return;
        }

        // Determine rectangle on screen
        const hoverBoundingRect = (findDOMNode(
            component,
        )).getBoundingClientRect();

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

        // Determine mouse position
        const clientOffset = monitor.getClientOffset();
        // Get pixels to the top
        const hoverClientY = (clientOffset).y - hoverBoundingRect.top;

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%
        // Dragging downwards


        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            return;
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            return;
        }

        // Time to actually perform the action
        props.moveCard(dragIndex, hoverIndex);

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex;
    }
};

class Card extends React.Component {
  static propTypes = {
      connectDragSource: PropTypes.func.isRequired,
      connectDropTarget: PropTypes.func.isRequired,
      connectPreview: PropTypes.func,
      moveSubCard: PropTypes.func,
      index: PropTypes.number.isRequired,
      isDragging: PropTypes.bool.isRequired,
      hovered: PropTypes.bool,
      subcards: PropTypes.array,
      id: PropTypes.any.isRequired,
      text: PropTypes.string.isRequired,
      moveCard: PropTypes.func.isRequired
  };

  componentDidMount() {
      this.props.connectPreview(getEmptyImage());
  }

  renderSubMenu = (subcards, indexParent) =>{
      return  subcards.map((card, i) => (
          <SubCard
              key={card.id}
              indexParent={indexParent}
              index={i}
              id={card.id}
              text={card.text}
              moveSubCard={this.props.moveSubCard}
          />
      ));
  };

  render() {
      const {
          text, index,
          isDragging,
          connectDragSource,
          connectDropTarget,
          hovered,
          subcards = []
      } = this.props;
      const opacity = isDragging ? 0 : 1;

      // const display = isDragging ? 'none' : 'block';
      const border =  hovered ? '1px solid green' : '1px dashed gray';


      return (
          <div>
              {
                  connectDragSource &&
                  connectDropTarget &&
                  connectDropTarget(
                      <div style={{position: 'relative'}}>
                          <div style={{ ...style, opacity, border }}>{text}</div>
                          {connectDragSource(<div style={{position: 'absolute', top: '35%', left: '10px', width: '10px', height: '10px', background: 'green', cursor: 'move', opacity}}/>)}

                      </div>,
                  )}
              {<div style={{opacity}}>{!!subcards.length && this.renderSubMenu(subcards, index)}</div>}
          </div>
      );
  }
}

export default flow(
    DragSource(
        'card',
        cardSource,
        (connect, monitor) => ({
            connectDragSource: connect.dragSource(),
            isDragging: monitor.isDragging(),
            connectPreview: connect.dragPreview()
        }),
    ),
    DropTarget(
        'card',
        cardTarget,
        (connect, monitor) => ({
            connectDropTarget: connect.dropTarget(),
            hovered: monitor.isOver()
        })
    )
)(Card);
