import React, { Component } from 'react';
import PropTypes from 'prop-types';
const style = {
    border: '1px dashed gray',
    padding: '16px 1rem',
    marginBottom: '.5rem',
    backgroundColor: 'white',
    boxSizing: 'border-box'
};
class CardDragPreview extends Component {
    static propTypes = {
        width: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired
    };

    render() {
        return (
            <div style={{ ...style, width: this.props.width }}>{this.props.text}</div>
        );
    }
}

export default CardDragPreview;
