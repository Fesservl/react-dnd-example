import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Item from './Item';
import Target from './Target';
import Card from './Card';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';
import CustomDragLayer from './CustomDragLayer';

class App extends Component {
  state = {
      items: [
          { id: 1, name: 'Item 1' },
          { id: 2, name: 'Item 2' },
          { id: 3, name: 'Item 3' },
          { id: 4, name: 'Item 4' }
      ],
      cards: [
          {
              id: 1,
              text: 'Write a cool JS library'
          },
          {
              id: 2,
              text: 'Make it generic enough'
          },
          {
              id: 3,
              text: 'Write README',
              subcards: [
                  {
                      id: 1,
                      text: '1. Write a cool JS library'
                  },
                  {
                      id: 2,
                      text: '2. Make it generic enough'
                  },
                  {
                      id: 3,
                      text: '3. Write README'
                  }
              ]
          },
          {
              id: 4,
              text: 'Create some examples'
          },
          {
              id: 5,
              text: 'Spam in Twitter and IRC to promote it (note that this element is taller than the others)'
          },
          {
              id: 6,
              text: '???'
          },
          {
              id: 7,
              text: 'PROFIT'
          }
      ]
  };

  deleteItem = id => {
      this.setState(prevState => {
          return { items: prevState.items.filter(item => item.id !== id) };
      });
  };

  moveSubCard = (dragIndex, hoverIndex, parentIndex) => {
      const { cards } = this.state;
      const subcards = [ ...cards[parentIndex].subcards];
      const dragCard = subcards[dragIndex];
      subcards[dragIndex] = subcards[hoverIndex];
      subcards[hoverIndex] = dragCard;
      const newCards = [ ...cards ];
      newCards[parentIndex].subcards = subcards;

      this.setState({cards: newCards});
  };

  moveCard = (dragIndex, hoverIndex) => {
      const { cards } = this.state;
      const dragCard = cards[dragIndex];
      const hoverCard = cards[hoverIndex];

	  const newCards = [ ...cards ];
	  newCards[dragIndex] = hoverCard;
	  newCards[hoverIndex] = dragCard;
	  
	  this.setState({cards: newCards});
  };

  render() {
      return (
          <div className='App'>
              <header className='App-header'>
                  <img src={logo} className='App-logo' alt='logo' />
                  <h1 className='App-title'>Welcome to React</h1>
              </header>
              <div className='App-intro'>
                  <div className='app-container'>
                      <div className='item-container'>
                          {this.state.items.map((item) => (
                              <Item key={item.id} item={item} handleDrop={(id) => this.deleteItem(id)} />
                          ))}
                      </div>
                      <Target />
                  </div>
                  <div className='card-container'>
                      {this.state.cards.map((card, i) => (
                          <Card
                              key={card.id}
                              index={i}
                              id={card.id}
                              text={card.text}
                              subcards={card.subcards}
                              moveCard={this.moveCard}
                              moveSubCard={this.moveSubCard}
                          />
                      ))}
                  </div>
              </div>
              <CustomDragLayer/>
          </div>
      );
  }
}

export default DragDropContext(HTML5Backend)(App);
