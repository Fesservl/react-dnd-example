import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import {getEmptyImage} from 'react-dnd-html5-backend';
import {DragSource, DropTarget} from 'react-dnd';
import flow from 'lodash/flow';

const style = {
    border: '1px dashed gray',
    padding: '0.5rem 1rem',
    marginBottom: '.5rem',
    // marginLeft: '20px',
    backgroundColor: 'white',
    cursor: 'move'
};

const cardSource = {
    beginDrag(props, monitor, component) {
        const hoverBoundingRect = (findDOMNode(
            component,
        )).getBoundingClientRect();
        return {
            id: props.id,
            index: props.index,
            text: props.text,
            width: hoverBoundingRect.width,
            left: hoverBoundingRect.left
        };
    }
};

const cardTarget = {
    hover(props, monitor, component) {
        const dragIndex = monitor.getItem().index;
        const hoverIndex = props.index;
        const parentIndex = props.indexParent;


        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return;
        }

        // Determine rectangle on screen
        const hoverBoundingRect = (findDOMNode(
            component,
        )).getBoundingClientRect();

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

        // Determine mouse position
        const clientOffset = monitor.getClientOffset();
        // Get pixels to the top
        const hoverClientY = (clientOffset).y - hoverBoundingRect.top;

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%
        // Dragging downwards


        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            return;
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            return;
        }

        // Time to actually perform the action
        props.moveSubCard(dragIndex, hoverIndex, parentIndex);

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex;
    }
};


class SubCard extends Component {
    componentDidMount() {
        this.props.connectPreview( getEmptyImage() );
    }
    render() {
        const {text, isDragging, connectDragSource, connectDropTarget} = this.props;
        const opacity = isDragging ? 0 : 1;
        return (
            connectDragSource &&
            connectDropTarget &&
            connectDropTarget(
                connectDragSource(
                    <div style={{position: 'relative', width: '500px'}}>
                        <div style={{ ...style, opacity}}>{text}</div>
                    </div>
                ))
        );
    }
}

SubCard.propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    connectPreview: PropTypes.func,
    index: PropTypes.number.isRequired,
    indexParent: PropTypes.number.isRequired,
    isDragging: PropTypes.bool.isRequired,
    id: PropTypes.any.isRequired,
    text: PropTypes.string.isRequired,
    moveSubCard: PropTypes.func.isRequired
};

export default flow(
    DragSource(
        'subcard',
        cardSource,
        (connect, monitor) => ({
            connectDragSource: connect.dragSource(),
            isDragging: monitor.isDragging(),
            connectPreview: connect.dragPreview()
        }),
    ),
    DropTarget('subcard', cardTarget, (connect, monitor) => ({
        connectDropTarget: connect.dropTarget(),
        hovered: monitor.isOver()
    }))
)(SubCard);
